'use strict';

angular.module('myApp.publicationsCtrl', ['ngRoute', 'ui.bootstrap'])

        .config(function ($routeProvider, $locationProvider) {
            $routeProvider
                    .when('/publications', {
                        templateUrl: 'publications/publications.html',
                        controller: 'publicationsCtrl'
                    })
                    .when('/publication/:publicationId', {
                        templateUrl: 'publications/publication-detail.html',
                        controller: 'publicationsCtrl'
                    })
                    .when('/publication/graph/test', {
                        templateUrl: 'publications/graph.html',
                        controller: 'publicationsCtrl'
                    })
                    .when('/publication/graph/venue', {
                        templateUrl: 'publications/graph_venue.html',
                        controller: 'publicationsCtrl'
                    }).when('/publication/:publicationId/graph/:type', {
                        templateUrl: 'publications/citation_network_revelant_pub.html',
                        controller: 'publicationsCtrl'
                    })                                            
                    .when('/citation-network', {
                        templateUrl: 'publications/citation_network_venue_select.html',
                        controller: 'publicationsCtrl'
                    })
                    .when('/citation-network/show', {
                        templateUrl: 'publications/citation_network_venue.html',
                        controller: 'publicationsCtrl'
                    });                       
        })

        .controller('publicationsCtrl', function ($scope, $http, $window, $routeParams, $route) {
            $scope.pubName = 'publication name';

            $scope.searchForm = {
                title: '',
                authorName: '',
                year: '',
                venue: '',
            };
            $scope.perPage = 5;
            $scope.currentPage = 1;
            $scope.totalItems = 0;
            $scope.publications = [];
            $scope.relevantPublications = [];
            $scope.searchRelevantTypesSelected = 0;
            $scope.searchRelevantTypesActual = 0;
            $scope.pub = null;
            $scope.biblicoupling = {name:'Bibliografická vazba', id:0, url: 'bibccoupling'};
            $scope.coocitation = {name: 'Kocitační vazba', id:1, url: 'coocitationindex'};
            $scope.coocitation2 = {name: 'Kocitační vazba dvouúrovňová', id:2, url: 'coocitationindex2'};
            $scope.coocitation3 = {name: 'Kocitační vazba tříúrovňová', id:3, url: 'coocitationindex3'};
            $scope.cpi = {name: 'Citation Proximity Index (CPA)', id:4, url: 'cpi'};
            $scope.options = {
                perPage: [5, 10, 25, 50, 100],
                relevantTypesOption: [$scope.biblicoupling, $scope.coocitation,
                    $scope.coocitation2, $scope.coocitation3, $scope.cpi],
                venues: ['COLING','HLT','ACL', 'NAACL','EACL', 'CL','IJCNLP','ANLP','MUC','INLG','EMNLP','LREC'],
                pubsCount: [5, 10, 25, 50, 100, 200, 500, 1000, 1500, 3000, 5000],
            };
            $scope.status = {
                open: true
            };
            
            $scope.showCitationNetwork = false;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                console.log('Page changed to: ' + $scope.currentPage);
                $scope.pubName = "changed";
                $scope.publications = $scope.publicationsAll.slice(($scope.currentPage - 1) * $scope.perPage, $scope.currentPage * $scope.perPage);
                //$scope.pub = $scope.publications[0]; // change
                console.log($scope.publicationsAll);
                console.log(($scope.currentPage - 1) * $scope.perPage);
                console.log($scope.currentPage * $scope.perPage);
            };

            $scope.refresh = function () {
                var dataObj = {
                    search: {
                        author: $scope.searchForm.authorName,
                        year: $scope.searchForm.year,
                        title: $scope.searchForm.title,
                        perPage: $scope.perPage,
                        page: $scope.currentPage,
                    }
                };
                $http.post('/rrs_citations/web/app_dev.php/api/v1/publications', dataObj).
                        success(function (data) {
                            console.log(data);
                            $scope.publications = data.result.hits.hits;
                            $scope.totalItems = data.result.hits.total;
                            
                        });

            };
            
            $scope.showCitationNetwork = function(){
                $window.location.href = '#citation-network/show';
                $route.reload();
            };
            
            $scope.getPublication = function (id) {
                $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/' + id).
                        success(function (data) {
                            console.log(data);
                            console.log(data.result.hits.hits.total);
                            if(data.result.hits.total > 0){
                                $scope.pub = data.result.hits.hits[0];
                                console.log($scope.pub);
                            }
                        });

            };
            
            $scope.loadCitedBy = function (id) {
                console.log('get cited by data');
                $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/' + id+'/citedby').
                        success(function (data) {
                            console.log(data);
                            console.log(data.result.hits.hits.total);
                            if(data.result.hits.total > 0){
                                $scope.pub.citedby = data.result.hits.hits;
                                console.log($scope.pub.citedby);
                            }
                        });

            };           
            
            $scope.searchRelevantPublications = function (id, searchTypeId) {
                console.log('search relevant pubs');
                console.log(searchTypeId);
                console.log($scope.cpi.id);
                
                var url = '';
                if(searchTypeId === $scope.biblicoupling.id){
                    url = $scope.biblicoupling.url;
                }else if(searchTypeId === $scope.coocitation.id){
                    url = $scope.coocitation.url;
                }else if(searchTypeId === $scope.coocitation2.id){
                    url = $scope.coocitation2.url;
                }else if(searchTypeId === $scope.coocitation3.id){
                    url = $scope.coocitation3.url;                    
                }else if(searchTypeId === $scope.cpi.id){
                    url = $scope.cpi.url;
                }else{
                    console.log('return');
                    return;
                }
                $http.get('/rrs_citations/web/app_dev.php/api/v1/publications/' + id+'/'+url).
                        success(function (data) {
                            console.log(data);
                            $scope.relevantPublications = data.result;
                            $scope.searchRelevantTypesActual = searchTypeId;
                        });

            };                       
            
            if ($routeParams.publicationId && $routeParams.type) {
                $scope.pubIdCitNetwork = $routeParams.publicationId;
                $scope.typeCitNetwork = $routeParams.type;
                
            }else if ($routeParams.publicationId) {
                //load single pub
                $scope.getPublication($routeParams.publicationId);
            } else {
                console.log("refresh");
                $scope.refresh();
                $scope.selectedVenue = 'ACL';
                $scope.selectedPubsCount = 25;
            }
        });