<?php

namespace RrsCitationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class testController extends Controller
{
     /**
     * @Route("/test/{name}", name="hello")
     */
    public function indexAction($name)
    {
        #return '<html><body>Hello '.$name.'!</body></html>';
        return 'hello';
    }
}
