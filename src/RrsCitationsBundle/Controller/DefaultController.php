<?php

namespace RrsCitationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Request;
use Elastica\Client;

class DefaultController extends Controller {

    private $index = 'xholik09_rrs_citations';
    private $host = 'athena1.fit.vutbr.cz';
    private $port = 9200;
    #private $docType = 'publication_acl_2';
    private $docType = 'publication_acl_2013_final';

    public function getTestAction(){
        return 'TEST';
    }
    
    private function elasticSearchRequest($query, $pathAppend = '_search') {
        $client = new Client(array(
            'host' => $this->host,
            'port' => $this->port
        ));
        #print_r(file_get_contents('http://athena1.fit.vutbr.cz:9200/xholik09_rrs_citations/publication/_search?size=100'));
        $url='http://'.$this->host.':'.$this->port.'/'.$this->index.'/'.$this->docType.'/'.$pathAppend;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        #print_r(json_encode($query, JSON_FORCE_OBJECT));            
        #return print_r(json_encode($query));
        #curl_setopt($ch,CURLOPT_POSTFIELDS,  json_encode($query, JSON_FORCE_OBJECT));
        curl_setopt($ch,CURLOPT_POSTFIELDS,  json_encode($query));
        #curl_setopt($ch,CURLOPT_POSTFIELDS,  $query);
        ob_start();
        curl_exec ($ch);
        curl_close ($ch);
        $data = ob_get_contents();
        ob_end_clean();
        #print_r($query);
        #print_r(json_decode($data,true));
        return json_decode($data,true);
        //return $client->request($path, Request::METHOD_GET, $query);
    }
    
    public function esGetCountOfDocuments(){
        $query = array('query' => array('match_all' => array()));
        $response = $this->elasticSearchRequest($query, '_count');
        //return $response->getData()['count'];       
        return $response['count'];        
    }
    
    
    public function esGetCitedByPublications($publicationId){
        
			$query = array(
			  "query" => array(
				"bool"=> array(
				  "must"=> array(
					  "nested"=> array(
						"path"=> "references", 
						"query"=> array(
						  "bool"=> array(
							"must"=> 
							  array( "match" => array( "references.referenced_pub_id" => $publicationId ))
					)))
				  )
			)));
                        #print_r($query);
                        
        //$response = $this->elasticSearchRequest($query);
        //return $response->getData();        
        return $this->elasticSearchRequest($query);
    }
    
    public function esGetPublicationById($publicationId){
        $query = array(
            'query' => array(
                'match' => array(
                    '_id' => $publicationId
                )
            )
        );
        //$response = $this->elasticSearchRequest($query);
        //return $response->getData();        
        return $this->elasticSearchRequest($query);
    }

    private function addRefToCocitationMap($ref, $coocitation){
        if($ref['title'] == null || $ref['title'] == ''){
            return $coocitation;
        }
        if(array_key_exists('referenced_pub_id' , $ref )){
            $key = $ref['referenced_pub_id'];
        }else{
            $year = ($ref['year'] != null)? $ref['year'] : '';
            $key = preg_replace('/\s+/', '', $ref['title']).''.$year;
        }
                        
        if(!array_key_exists($key , $coocitation )){
            if(array_key_exists('referenced_pub_id' , $ref )){
                $coocitation[$key] = $this->esGetPublicationById($ref['referenced_pub_id'])['hits']['hits'][0];
            }else{
                $coocitation[$key]['_source'] = $ref;
            }
            $coocitation[$key]['cocitIndex'] = 1;
        }else{
            $coocitation[$key]['cocitIndex'] = $coocitation[$key]['cocitIndex'] + 1;
        }
        return $coocitation;        
    }    
    
    /**
     *     
     * @Route /publications
     * @param  Request $request json request
     * @return array of publication 
     */
    public function postPublicationsAction(Request $request) {
        $search = $request->request->get('search');
        $params = array();
        $content = $this->get("request")->getContent();
        if (!empty($content)) {
            $params = json_decode($content, true);
            $search = $params['search'];
            $title = (array_key_exists('title', $search)) ? $search['title'] : null;
            $author = (array_key_exists('author', $search)) ? $search['author'] : null;
            $year = (array_key_exists('year', $search)) ? $search['year'] : null;
            $perPage = (array_key_exists('perPage', $search)) ? $search['perPage'] : null;
            $venue = (array_key_exists('venue', $search)) ? $search['venue'] : null;
            $page = (array_key_exists('page', $search)) ? $search['page'] : null;

            $querySearch = [];
            if($title != null && $title != ''){
                $querySearch[] = array( 'match' => array('title' => $title));
            }
            if($author != null && $author != ''){
                $querySearch[] = array( 'match' => array('title' => $author));
            }
            if($year != null && $year != ''){
                $querySearch[] = array( 'match' => array('title' => $year));
            }
            
            if($venue != null && $venue != ''){
                $querySearch[] = array( 'match' => array('venue_abbr' => $venue));
            }            
                
            $match = array('match_all'=> array());
            if(count($querySearch)>0){
                $match = array( "bool" => array( 'should'=> $querySearch ));
            }
            $query = array(
                'query' => $match
                ,
                'from' => ($page - 1) * $perPage,
                'size' => $perPage,
            );                 
            $response = $this->elasticSearchRequest($query);
            return array(
                'result' => $response,
                'total' => $this->esGetCountOfDocuments()    
                    );
        }
        return array('result' => '');
    }

    /*
     * @Route /publications/{publicationId}
     */
    public function getPublicationsAction($publicationId) {
        //return $this->container->get('doctrine.entity_manager')->getRepository('Page')->find($id);
        //http://athena1.fit.vutbr.cz:920/_cluster/state/cluster_name%2Cnodes%2Cmaster_node%2Cversion
        //$id = $paramFetcher->get('id');
        //$result = file_get_contents('http://athena1.fit.vutbr.cz:9200/_cluster/state/cluster_name%2Cnodes%2Cmaster_node%2Cversion');
        #print '??? ';
        $data = $this->esGetPublicationById($publicationId);
        #print_r($data['hits']['hits'][0]);
        #print '??? ';
        $citedBy = $this->esGetCitedByPublications($publicationId);
       # print_r($citedBy);
        #print '??? ';
        if($data['hits']['total'] > 0){
            $data['hits']['hits'][0]['_source']['citedby'] = $citedBy['hits']['hits'];
        }
        return array('result' => $data);

        #return $resultSet = $search->search(); // returns Elastica\ResultSet
        #$result = file_get_contents('http://athena1.fit.vutbr.cz:9200/xholik09_rrs_citations/publication/_search?size=100');
        #return array('result' => (array) json_decode($result,true));
    }
    
    /*
     * @Route /publications/{publicationId}/citedby
     */    
    public function getPublicationsCitedbyAction($publicationId) {
        return array('result' => $this->esGetCitedByPublications($publicationId));
    }

    /*
     * @Route /publications/{publicationId}/coocitationindex
     */    
    public function getPublicationsCoocitationindexAction($publicationId) { 
        $citedByPubs = $this->esGetCitedByPublications($publicationId);
        $coocitation = array();
        foreach($citedByPubs['hits']['hits'] as $citedByPub){
            foreach($citedByPub['_source']['references'] as $ref){
                if(array_key_exists('referenced_pub_id' , $ref ) && $publicationId != $ref['referenced_pub_id']){
                    $coocitation = $this->addRefToCocitationMap($ref, $coocitation);
                }
            }
        }
        return array('result' => $coocitation);
    
    } 
    
    /*
     * @Route /publications/{publicationId}/coocitationindex2
     * 
     * 	Three level co-ocitation similarity
     * Find relevant documetns(Dx) for doc Dq where
     * Dk cites Dq, Di cites Dk and Dx cites Di
     * 	
     * 	       Di
     *        /	 \
     * 	    Dk    \
     * 	   /       \
     * 	 Dq        Dx 
     */
    
    
    public function getPublicationsCoocitationindex2Action($publicationId) { 
        $pubsCitedBy = $this->esGetCitedByPublications($publicationId);
        $coocitation = array();
        foreach($pubsCitedBy['hits']['hits'] as $pubCitedBy){
            
            foreach($pubCitedBy['_source']['references'] as $ref){
                if(array_key_exists('referenced_pub_id' , $ref ) && $publicationId != $ref['referenced_pub_id']){
                    $coocitation = $this->addRefToCocitationMap($ref, $coocitation);
                }
            }
            
            $pubsCitedBySec = $this->esGetCitedByPublications($pubCitedBy['_id']);
            foreach($pubsCitedBySec['hits']['hits'] as $pubCitedBySec){
                foreach($pubCitedBySec['_source']['references'] as $ref){
                    if(array_key_exists('referenced_pub_id' , $ref ) && $pubCitedBy['_id'] != $ref['referenced_pub_id']){
                        $coocitation = $this->addRefToCocitationMap($ref, $coocitation);
                    }
                }
            }
        }
        return array('result' => $coocitation);
    
    }     
    
    
    /*
     * @Route /publications/{publicationId}/coocitationindex2
     * 
     *	Three level co-ocitation similarity second type
     *	Find relevant documetns(Dx) for doc Dq where
     *	Dk cites Dq, Di cites Dk,j and Dj cites Dx
     *	
     *	         Di
     *          /  \
     *	      Dk    Dj
     *       /        \
     *      Dq         Dx 
     */    
    public function getPublicationsCoocitationindex3Action($publicationId) { 
        $pubsCitedBy = $this->esGetCitedByPublications($publicationId);
        $coocitation = array();
        foreach($pubsCitedBy['hits']['hits'] as $pubCitedBy){
            foreach($pubCitedBy['_source']['references'] as $ref){
                if(array_key_exists('referenced_pub_id' , $ref ) && $publicationId != $ref['referenced_pub_id']){
                    $coocitation = $this->addRefToCocitationMap($ref, $coocitation);
                }
            }
            
            $pubsCitedBySec = $this->esGetCitedByPublications($pubCitedBy['_id']);
            foreach($pubsCitedBySec['hits']['hits'] as $pubCitedBySec){
                foreach($pubCitedBySec['_source']['references'] as $ref){
                    if(array_key_exists('referenced_pub_id' , $ref ) && $pubCitedBy['_id'] != $ref['referenced_pub_id']){
                        $coocitation = $this->addRefToCocitationMap($ref, $coocitation);
                        $pubDj = $this->esGetPublicationById($ref['referenced_pub_id'])['hits']['hits'][0];
                        foreach($pubDj['_source']['references'] as $refPubDj){
                            $coocitation = $this->addRefToCocitationMap($refPubDj, $coocitation);
                        }
                    }
                }
            }
        }
        return array('result' => $coocitation);
    
    }    
    
    /*
     * @Route /publications/{publicationId}/bibccoupling
     */    
    public function getPublicationsBibccouplingAction($publicationId) {

        $data = $this->esGetPublicationById($publicationId);
        if($data['hits']['total'] > 0){
            $publication = $data['hits']['hits'][0];
            
            
            $refsPubsQuery = array();
            $refsPubs = array();
            foreach($publication['_source']['references'] as $ref){
                if(array_key_exists ('referenced_pub_id' , $ref )){
                   $refsPubsQuery[] = array("match" => array( "references.referenced_pub_id" => $ref['referenced_pub_id']));
                   $refsPubs[] = $ref['referenced_pub_id'];
                }
                if($ref['title'] != null){
                    $refsPubsQuery[] = array("match_phrase"=> array( "references.title.std"=> array("query"=> $ref['title'],"boost"=> 2)));
                }
            }
            if(count($refsPubsQuery) > 0){
                $query = array(
                    "query" => array(
                        "nested"=> array(
                            "inner_hits" => (object)array(),
                            "path"=> "references", 
                            "query" => array(
                                "bool" => array(
                                    "should"=> $refsPubsQuery,
                                    "must_not"=> array("ids" => array( "values" => array($publicationId))),
                                    "minimum_should_match" => 1 
                                )
                             ),
                        )
                     )
                    );    
                $query2 = array(
                    "query" => array(
                        "nested"=> array(
                            "inner_hits" => array(),
                            "path"=> "references", 
                            "query" => array(
                                "match" => array( 'references.referenced_pub_id'=> 'AVRu0BLotut5FjxJ9d9F')
                                )
                             )
                        )
                     
                    );                    
                
                #print_r($query);
                $response = $this->elasticSearchRequest($query);
                
                $resBibCoupl = $response;
                #print_r($response);
                //print_r($resBibCoupl);
                $responseRelevant = [];
                #print_r($refsPubs);
                foreach($resBibCoupl['hits']['hits'] as $bibCoupl){
                    #print $bibCoupl['_id'];
                    #print $bibCoupl['_score'];
                    #print $bibCoupl['_index'];
                    #print '@@@@@@@@@@@@@@@@@@@@';
                    //print_r($bibCoupl);
                    $bibCoupl['_source']['bib_coupling_pubs_id'] = [];
                    foreach($bibCoupl['inner_hits']['references']['hits']['hits'] as $ref){
                        $bibCoupl['_source']['bib_coupling_pubs_id'][] = $ref['_nested']['offset'];
                    }
                    $responseRelevant[] = $bibCoupl;
                }
                //print_r($responseRelevant);
                return array('result' => $responseRelevant);
            }
        }
        
        return array('result' => '');

    }    
    
     /*
     * @Route /publications/{publicationId}/cpi
     */    
    public function getPublicationsCpiAction($publicationId) {
        
        $citedByPubs = $this->esGetCitedByPublications($publicationId);
        $resultList = array();
        foreach($citedByPubs['hits']['hits'] as $citedByPub){
            if(!array_key_exists('references' , $citedByPub['_source'] )){
                continue;
            }
            $refPubOrig = null;
            foreach($citedByPub['_source']['references'] as $ref){
                if(array_key_exists('referenced_pub_id' , $ref ) && $publicationId == $ref['referenced_pub_id']){
                    $refPubOrig = $ref;
                }
            }

            # citedby similiar pubs
            if($refPubOrig == null){
                continue;
            }
            foreach($citedByPub['_source']['references'] as $ref){
                if(!array_key_exists('referenced_pub_id' , $ref )){
                    continue;
                }
                if($ref['referenced_pub_id'] == $publicationId){
                    continue;
                }
                
                # find CPI
                # same sentence 	- 1
                # same paragraph 	- 1/2  
		# same section		- 1/4
		# same publication	- 1/8
		$currentCpi = 0.125;
                foreach($ref['citation_contexts'] as $coContext){
                    foreach($refPubOrig['citation_contexts'] as $ccOrig){
                        if($ccOrig['context'] == $coContext['context']){
                            $currentCpi = 1;
                        }
                        if($ccOrig['section'] == $coContext['section'] && $currentCpi < 0.25){
                            $currentCpi = 0.25;
                        }                        
                    }
                    
                }
                $data = ['cpi'=> $currentCpi, 'citingDocId'=> $citedByPub['_id']];
                if(!array_key_exists($ref['referenced_pub_id'] , $resultList )){
                    $resultList[$ref['referenced_pub_id']] = array();
                    $resultList[$ref['referenced_pub_id']]['results'] = array($data);
                    $resultList[$ref['referenced_pub_id']]['cpiTotal'] = $currentCpi;
                }else{
                    $resultList[$ref['referenced_pub_id']]['results'][] = $data;
                    $resultList[$ref['referenced_pub_id']]['cpiTotal'] += $currentCpi;
                }                
            }    
        } 
        
        // load found publications
        $result = [];
        $keys = array_keys($resultList);
        foreach($keys as $key){
            $data = $this->esGetPublicationById($key);
            if($data['hits']['total'] > 0){
                $pub = $data['hits']['hits'][0];
                $pub['cpi'] = $resultList[$key]['cpiTotal'];
                $result[] = $pub;
            }
        }
        return array('result' => $result);
    }
    
    
    /*
     * GRAPHS
     */
    
    
    /*
     * @Route  /publications/{id}/graphs/{type}
     * show citation network for publication by id and show relevant documents based on 
     * selected type   
     */    
    public function getPublicationGraphAction($id, $type){
         $data = $this->esGetPublicationById($id);
            if($data['hits']['total'] > 0){
                $pubOrig = $data['hits']['hits'][0];
            }else{
                return;
            }   
            
            
         if($type == 0){
             $relevantDocuments = $this->getPublicationsBibccouplingAction($id);
         }elseif($type == 1){
             $relevantDocuments = $this->getPublicationsCoocitationindexAction($id);
         }elseif($type == 2){
             $relevantDocuments = $this->getPublicationsCoocitationindex2Action($id);
         }elseif($type == 3){ 
             $relevantDocuments = $this->getPublicationsCoocitationindex3Action($id);
         }elseif($type == 4){
             $relevantDocuments = $this->getPublicationsCpiAction($id);
         }
         $relevantDocuments = $relevantDocuments['result'];
         #$relevantDocuments = array();
         $relevantDocuments[] = $pubOrig;
         
         #print_r(count($relevantDocuments));
         #print_r($relevantDocuments);

         foreach ($relevantDocuments as $key => $value) {
                $citedBy = $this->esGetCitedByPublications($relevantDocuments[$key]['_id']);
                # print_r($citedBy);
                 #print '??? ';
                 if($data['hits']['total'] > 0){
                     $relevantDocuments[$key]['_source']['citedby'] = $citedBy['hits']['hits'];
                 }             
         }
         
        $graphData =  $this->createGraph($relevantDocuments, false);
        
        $i = 0;
        foreach ($graphData['graph']['nodes'] as $node) {
            if($node['id'] == $pubOrig['_id']){
                $graphData['graph']['nodes'][$i]['color'] = '#ff00db';
               # print "ORIG FOUND";
            }
            foreach ($relevantDocuments as $relpub) {
                if($node['id'] == $relpub['_id'] && $pubOrig['_id'] != $relpub['_id']){
                    $graphData['graph']['nodes'][$i]['color'] = '#00fff9';
                }
            }
            
            
           $i++; 
        }    
        
        
        return array(
                'graphData' => $graphData,
                #'total' => $this->esGetCountOfDocuments()    
        );             
    }    


     /*
     * @Route  /publications/{venue}/citnetworks/{size}
     */    
    public function getPublicationCitnetworkAction($venue, $size = 50){
             
            $match = array('match' => array('venue_abbr' => $venue));
            #$match = array( "bool" => array( 'must'=> array( 'match' => array('_id' => 'AVSK3aoetut5FjxJ_MWW'))));
            $query = array(
                'query' => $match,
                    'size'=>$size
            );                 
            
            $response = $this->elasticSearchRequest($query);
            
            
            #print_r(count($response['hits']['hits']));print '  & '; print $venue;
            #return array();
            for($i=0; $i < count($response['hits']['hits']); $i++) {
                $citedBy = $this->esGetCitedByPublications($response['hits']['hits'][$i]['_id']);
                $response['hits']['hits'][$i]['_source']['citedby'] = $citedBy['hits']['hits'];
            }
            #print_r($response['hits']['hits']);
            
            $graphData =  $this->createGraph($response['hits']['hits'], true);
            
            
            return array(
                'graphData' => $graphData,
                'total' => $this->esGetCountOfDocuments()    
                    );
            return array('result' => array('graph'=> 'nodes'));
    }
    
    
    private function createGraphNode($title, $authors, $id, $venue, $setColor = false, $cpi = 0){
        $color = '#66CC33';
	if($venue == 'COLING'){
            $color = '#7e1203';
        }
	if($venue == 'HLT'){
            $color = '#ff00db';
        }
	if($venue == 'ACL'){
            $color = '#00fff9';
        }
	if($venue == 'NAACL'){
            $color = '#726d00';
        }
	if($venue == 'EACL'){
            $color = '#97b400';
        }
	if($venue == 'CoNLL'){
            $color = '#ff0000';
        }
	if($venue == 'CL'){
            $color = '#edae0e';
        }
        if($venue == 'IJCNLP'){
            $color = '#9278e6';
        }
	if($venue == 'ANLP'){
            $color = '#36342a';
        }
	if($venue == 'MUC'){
            $color = '#d8ffff';
        }
	if($venue == 'INLG'){
            $color = '#e7d6ff';
        }
	if($venue == 'EMNLP'){
            $color = '#6c4c56';
        }
	if($venue == 'LREC'){
            $color = '#a7a78f';
        }
        if(!$setColor){
            $color = '#66CC33';
        }
        
        $pub = array(
                    'title'=> $title,
                    'authors'=> $authors,
                    'id'=> $id,
                    'venue_abbr'=> $venue,
                    "type"=> "circle",
                    "size"=> 20,
                    "color"=> $color,
                    "cpi"=> $cpi
        );        
        return $pub;
    }
    
    private function createGraphLink($source, $target){
        return array('source'=>$source, 'target'=>$target);
    }    
    
    private function createGraph($publications, $setColor = false){
        $nodes = array();
        $links = array();
        $pubNodes = array();
        #print_r(count($publications));
        #print_r($publications);
        
        foreach ($publications as $pub) {
         #   print_r($pub);
            if(!array_key_exists($pub['_id'] , $pubNodes )){
                $venueAbbr = $pub['_source']['venue'];
                if(array_key_exists('venue_abbr' , $pub['_source'] )){
                     $venueAbbr = $pub['_source']['venue_abbr'];
                } 
                $cpi = '';
                if(array_key_exists('cpi' , $pub )){
                    $cpi = $pub['cpi'];
                }
                $nodes[] = $this->createGraphNode($pub['_source']['title'], $pub['_source']['authors'], $pub['_id'], $venueAbbr, $setColor, $cpi);
                $pubNodes[$pub['_id']] = count($nodes)-1;
            }
            
            foreach ($pub['_source']['references'] as $ref) {
                $id = null;
                if(!array_key_exists('referenced_pub_id' , $ref )){
                    if(!array_key_exists($ref['title'] , $pubNodes )){
                        $nodes[] = $this->createGraphNode($ref['title'], $ref['authors'], null, null, $setColor);
                        $pubNodes[$ref['title']] = count($nodes) -1;
                    }
                }else if(!array_key_exists($ref['referenced_pub_id'] , $pubNodes )){
                    $nodes[] = $this->createGraphNode($ref['title'], $ref['authors'], $ref['referenced_pub_id'], null, $setColor);
                    $pubNodes[$ref['referenced_pub_id']] = count($nodes) -1;
                }
            }
            #print_r($pub['_source']);
            if(array_key_exists('citedby' , $pub['_source'] )){
                foreach ($pub['_source']['citedby'] as $citedby) {
                    if(!array_key_exists($citedby['_id'] , $pubNodes )){
                        $venueAbbr = $citedby['_source']['venue'];
                        if(array_key_exists('venue_abbr' , $citedby['_source'] )){
                            $venueAbbr = $citedby['_source']['venue_abbr'];
                        }
                        $nodes[] = $this->createGraphNode($citedby['_source']['title'], $citedby['_source']['authors'], $citedby['_id'], $venueAbbr, $setColor);
                        $pubNodes[$citedby['_id']] = count($nodes)-1;
                    }
                }
            }
        }
        
        
        foreach ($publications as $pub) {
            $pubIndex = $pubNodes[$pub['_id']];
            
            foreach ($pub['_source']['references'] as $ref) {
                if(!array_key_exists('referenced_pub_id' , $ref )){
                    $refIndex = $pubNodes[$ref['title']];
                }else{
                    $refIndex = $pubNodes[$ref['referenced_pub_id']];
                }
                $links[] = $this->createGraphLink($pubIndex, $refIndex);
            }
            
            if(array_key_exists('citedby' , $pub['_source'] )){
                foreach ($pub['_source']['citedby'] as $citedby) {
                        $citedbyIndex = $pubNodes[$citedby['_id']];
                        $links[] = $this->createGraphLink($citedbyIndex ,$pubIndex);
                }
            }
        }        
        
        return array('graph'=>array('nodes'=>$nodes, 'links'=> $links, 'pubNodes'=>$pubNodes));
        
    }

    
}
